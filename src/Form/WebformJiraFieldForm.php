<?php

namespace Drupal\webform_jira_service_desk\Form;

use Drupal\Component\Utility\Unicode;
use Drupal\Core\Entity\BundleEntityFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Url;
use Drupal\webform\Form\WebformDialogFormTrait;
use Drupal\webform\Utility\WebformYaml;
use Drupal\webform\WebformTokenManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class Webform Jira Field Form.
 *
 * @package Drupal\webform_jira_service_desk\Form
 */
class WebformJiraFieldForm extends BundleEntityFormBase {

  use WebformDialogFormTrait;

  /**
   * Messenger.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Webform token manager.
   *
   * @var \Drupal\webform\WebformTokenManagerInterface
   */
  protected $webformTokenManager;

  /**
   * Jira field.
   *
   * @var array
   */
  protected $jiraFields;

  /**
   * WebformJiraFieldForm constructor.
   *
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   Messenger.
   * @param \Drupal\webform\WebformTokenManagerInterface $webform_token_manager
   *   Webform token manager.
   */
  public function __construct(MessengerInterface $messenger, WebformTokenManagerInterface $webform_token_manager) {
    $this->messenger = $messenger;
    $this->webformTokenManager = $webform_token_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('messenger'),
      $container->get('webform.token_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\webform\WebformInterface $webform */
    $webform = $this->getEntity();
    $serviceDeskId = $webform->getThirdPartySetting('webform_jira_service_desk', 'servicedesk_id');
    $requestTypeId = $webform->getThirdPartySetting('webform_jira_service_desk', 'request_type_id');

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#description' => $this->t('Administrative label.'),
      '#required' => TRUE,
    ];
    $form['key'] = [
      '#type' => 'machine_name',
      '#title' => $this->t('Key'),
      '#machine_name' => [
        'label' => '<br/>' . $this->t('Key'),
        'exists' => [$this, 'exists'],
        'source' => ['label'],
      ],
      '#required' => TRUE,
    ];

    $form['mapping'] = [
      '#title' => $this->t('JIRA mapping information'),
      '#type' => 'details',
      '#open' => TRUE,
      '#attributes' => [
        'class' => ['field-value-information'],
      ],
    ];

    $form['mapping']['field_id'] = [
      '#type' => 'select',
      '#title' => $this->t('Jira field'),
      '#options' => $this->getJiraFieldOptions($serviceDeskId, $requestTypeId),
      '#required' => TRUE,
    ];

    $form['value'] = [
      '#title' => $this->t('Value information'),
      '#type' => 'details',
      '#open' => TRUE,
      '#attributes' => [
        'class' => ['field-value-information'],
      ],
    ];

    $form['value']['value_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Field value type'),
      '#options' => [
        'webform_element' => $this->t('Webform element'),
        'text' => $this->t('Text'),
      ],
      '#required' => TRUE,
    ];

    $form['value']['text'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Text value'),
      '#states' => [
        'visible' => [
          ':input[name="value_type"]' => ['value' => 'text'],
        ],
        'required' => [
          ':input[name="value_type"]' => ['value' => 'text'],
        ],
      ],
    ];

    if ($token_tree = $this->webformTokenManager->buildTreeElement()) {
      $form['value']['token_tree_link'] = $token_tree;
      $form['value']['token_tree_link']['#states'] = [
        'visible' => [
          ':input[name="value_type"]' => ['value' => 'text'],
        ],
        'required' => [
          ':input[name="value_type"]' => ['value' => 'text'],
        ],
      ];
    }

    $form['value']['webform_element'] = [
      '#type' => 'select',
      '#title' => $this->t('Webform element'),
      '#options' => $this->getWebformElementOptions(),
      '#description' => $this->t('Select the webform element. These elements can be found at the elements section.'),
      '#states' => [
        'visible' => [
          ':input[name="value_type"]' => ['value' => 'webform_element'],
        ],
        'required' => [
          ':input[name="value_type"]' => ['value' => 'webform_element'],
        ],
      ],
    ];

    $form = parent::buildForm($form, $form_state);
    if (!empty($form['actions']['delete'])) {
      unset($form['actions']['delete']);
    }
    return $this->buildDialogForm($form, $form_state);
  }

  /**
   * Validate the form ensuring field names are not duplicated.
   *
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $field_id = $form_state->getValue('field_id');
    $key = $form_state->getValue('key');
    if ($this->checkFieldExistenceByFieldId($field_id, $key)) {
      $form_state->setErrorByName('field_id', $this->t(
        "Field id <strong>@field_id</strong> already exist please choose a different field id",
        ['@field_id' => $form_state->getValue('field_id')]
      ));
    }

    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $webform = $this->getEntity();
    $form_state->setRedirectUrl(Url::fromRoute(
      'webform_jira_service_desk.configuration',
      ['webform' => $webform->id()]
    ));
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {

    /** @var \Drupal\webform\WebformInterface $webform */
    $webform = $this->getEntity();

    $fields = $this->processField($form_state) + $this->getFields();

    $webform->setThirdPartySetting(
      'webform_jira_service_desk',
      'fields',
      WebformYaml::encode($fields)
    );
    $this->setEntity($webform);
    $this->messenger->addStatus($this->t(
      'Jira mapping saved for @label.',
      ['@label' => $webform->label()]
    ));

    parent::save($form, $form_state);
  }

  /**
   * Create option list based on JIRA fields.
   *
   * @param int|string $serviceDeskId
   *   Service desk id.
   * @param int|string $requestTypeId
   *   Request type id.
   *
   * @return array
   *   Options array.
   */
  protected function getJiraFieldOptions($serviceDeskId, $requestTypeId) {
    /** @var \Drupal\webform_jira_service_desk\JiraServiceDeskService */
    $jira = \Drupal::service('webform_jira_service_desk.jira_service');
    $this->jiraFields = $jira->getRequestTypeFields($serviceDeskId, $requestTypeId);
    $options = [];
    foreach ($this->jiraFields as $f) {
      $options[$f['id']] = $f['display'];
    }
    return $options;
  }

  /**
   * Get options list for Webform fields.
   *
   * @return array
   *   Element options.
   */
  protected function getWebformElementOptions() {
    $element_options = [];
    /** @var \Drupal\webform\WebformInterface $webform */
    $webform = $this->getEntity();
    $decoded_elements = $webform->getElementsDecodedAndFlattened();
    if ($decoded_elements) {
      foreach ($decoded_elements as $key => $element) {
        $element_options[$key] = isset($element['#title']) ? Unicode::truncate($element['#title'], 30, TRUE, TRUE) . ' (key: ' . $key . ')' : $element['#type'];
      }
    }
    return $element_options;
  }

  /**
   * Get list of defined JIRA field mappings.
   *
   * @return array
   *   Webform YAML array.
   */
  protected function getFields() {
    /** @var \Drupal\webform\WebformInterface $webform */
    $webform = $this->getEntity();
    return (array) WebformYaml::decode($webform->getThirdPartySetting(
      'webform_jira_service_desk',
      'fields'
    ));
  }

  /**
   * Get a single field by Machine Key.
   *
   * @param string $key
   *   Key.
   *
   * @return array|null
   *   Field array.
   */
  protected function getFieldByKey(string $key) {
    $field = NULL;
    $fields = $this->getFields();
    if (isset($fields[$key])) {
      $field = $fields[$key];
    }
    return $field;
  }

  /**
   * Convert form state to storage format.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state.
   *
   * @return array
   *   Field array.
   */
  protected function processField(FormStateInterface $form_state) {
    $new_field = [];
    $value_type = $form_state->getValue('value_type');
    $fieldId = $form_state->getValue('field_id');
    $new_field[$form_state->getValue('key')] = [
      'key' => $form_state->getValue('key'),
      'label' => $form_state->getValue('label'),
      'value_type' => $value_type,
      'value' => $form_state->getValue($value_type),
      'field_type' => $this->getJiraType($fieldId),
      'field_id' => $form_state->getValue('field_id'),
    ];

    return $new_field;
  }

  /**
   * Get field type from JIRA field definition.
   *
   * @param int|string $id
   *   Jira id.
   *
   * @return string
   *   Field type.
   */
  protected function getJiraType($id) {
    foreach ($this->jiraFields as $field) {
      if ($field['id'] == $id) {
        return $field['type'];
      }
    }
  }

  /**
   * Check if another webform field is using this JIRA mapping.
   *
   * @param int $field_id
   *   JIRA field name.
   * @param string $key
   *   Machine Key of the expected field.
   *
   * @return bool
   *   TRUE if another Webform element is mapped to this JIRA field.
   */
  protected function checkFieldExistenceByFieldId($field_id, $key) {
    $fields = $this->getFields();
    foreach ($fields as $field) {
      if ($field['field_id'] == $field_id && $field['key'] != $key) {
        return TRUE;
      }
    }

    return FALSE;
  }

  /**
   * Machine Key existence.
   *
   * @param string $key
   *   Machine Key.
   *
   * @return bool
   *   True/False.
   */
  public function exists($key) {
    return $this->getFieldByKey($key) ? TRUE : FALSE;
  }

}
