<?php

namespace Drupal\webform_jira_service_desk\Form;

use Drupal\Core\Datetime\DateFormatter;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\Messenger;
use Drupal\Core\Queue\QueueFactory;
use Drupal\webform\Entity\WebformSubmission;
use Drupal\webform_jira_service_desk\JiraServiceDeskQueueHealthService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Queue Health Form.
 */
class QueueHealthForm extends FormBase {

  /**
   * Messenger.
   *
   * @var \Drupal\Core\Messenger\Messenger
   */
  protected $messenger;

  /**
   * Queue Factory.
   *
   * @var \Drupal\Core\Queue\QueueFactory
   */
  protected $queueFactory;

  /**
   * Jira Service Desk Queue Health Service.
   *
   * @var \Drupal\webform_jira_service_desk\JiraServiceDeskQueueHealthService
   */
  protected $queueHealthService;

  /**
   * Date formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatter
   */
  protected $dateFormatter;

  /**
   * Constructs a queue health form.
   *
   * @param \Drupal\webform_jira_service_desk\JiraServiceDeskQueueHealthService $queueHealthService
   *   Jira Service Desk Queue Health Service.
   * @param \Drupal\Core\Datetime\DateFormatter $dateFormatter
   *   Date formatter.
   * @param \Drupal\Core\Queue\QueueFactory $queueFactory
   *   Database connection.
   * @param \Drupal\Core\Messenger\Messenger $messenger
   *   Messenger.
   */
  public function __construct(JiraServiceDeskQueueHealthService $queueHealthService, DateFormatter $dateFormatter, QueueFactory $queueFactory, Messenger $messenger) {
    $this->queueHealthService = $queueHealthService;
    $this->dateFormatter = $dateFormatter;
    $this->queueFactory = $queueFactory;
    $this->messenger = $messenger;

  }

  /**
   * Creates Queue Health form.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   Container.
   *
   * @return \Drupal\webform_jira_service_desk\Form\QueueHealthForm|static
   *   Queue Health form.
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('webform_jira_service_desk.queue_health_service'),
      $container->get('date.formatter'),
      $container->get('queue'),
      $container->get('messenger'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'webform_jira_service_desk_queue_health';

  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $content = [
      '#theme' => 'item_list',
      '#list_type' => 'ul',
      '#title' => 'Summary',
      '#wrapper_attributes' => ['class' => 'container'],
    ];

    $last_success = $this->queueHealthService->getLastSuccess();
    if ($last_success) {
      $content['#items'][] = 'Last Success: ' . $this->dateFormatter->format($last_success);
    }
    $content['#items'][] = 'There are ' . $this->queueHealthService->getRemainingCount() . ' items in the queue.';
    if ($this->queueHealthService->getOldestItemCreated()) {
      $content['#items'][] = 'The oldest item was created: ' . $this->dateFormatter->format($this->queueHealthService->getOldestItemCreated());
    }

    $form['summary'] = $content;

    $form['send_attempts_title'] = [
      '#markup' => '<h3>Unsuccessful Send to Jira attempts</h3>',
    ];

    $form['send_attempts'] = [
      '#type' => 'table',
      '#tableselect' => TRUE,
      '#title' => 'Unsuccessful send attempts',
      '#header' => [
        'date' => $this->t('Date'),
        'webform_form' => $this->t('Webform Form'),
        'submission_id' => $this->t('Submission ID'),
        'response_code' => $this->t('Response Code'),
        'message' => $this->t('Message'),
      ],
    ];

    $failures = $this->queueHealthService->getResendableFailures();
    foreach ($failures as $failure) {
      $row = [
        'date' => [
          '#markup' => $this->dateFormatter->format($failure->timestamp, 'short'),
        ],
        'webform_form' => [
          '#markup' => $failure->webform_form,
        ],
        'submission_id' => [
          '#markup' => $failure->webform_sid,
        ],
        'response_code' => [
          '#markup' => $failure->response_code,
        ],
        'message' => [
          '#markup' => $failure->response_message,
        ],
      ];
      // This will overwrite multiple failures of the same form, but I figure it
      // Can only be re-queued once.
      $form['send_attempts'][$failure->webform_sid] = $row;
    }

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('ReQueue'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValue('send_attempts');
    $queue = $this->queueFactory->get('cron_jira_request_queue');
    foreach ($values as $key => $value) {
      if ($value) {
        $webform_submission = WebformSubmission::load($key);
        $queue->createItem($webform_submission);
        $this->messenger->addMessage('Submission ID ' . $key . ' requeued.');
      }
    }
  }

}
