<?php

namespace Drupal\webform_jira_service_desk\Form;

use Drupal\Component\Utility\Unicode;
use Drupal\Core\Entity\BundleEntityFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\webform\Form\WebformEntityAjaxFormTrait;
use Drupal\webform\Utility\WebformDialogHelper;
use Drupal\webform\Utility\WebformYaml;

/**
 * Form builder for Jira Service Desk configuration.
 *
 * @package Drupal\webform_jira_service_desk\Form
 */
class WebformJiraConfigurationForm extends BundleEntityFormBase {
  use WebformEntityAjaxFormTrait;

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $options_service_desk = $this->getServiceDeskOptions();
    if ($options_service_desk) {

      /** @var \Drupal\webform\WebformInterface $webform */
      $webform = $this->getEntity();

      $form['jira_configuration'] = [
        '#type' => 'details',
        '#title' => 'JIRA Service Desk configuration',
        '#open' => FALSE,
        '#attributes' => [
          'class' => ['jira-mapping'],
        ],
      ];

      $form['jira_configuration']['jira_status'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Activate Jira service'),
        '#description' => $this->t('Submission data will be sent to JIRA Service Desk if activated.'),
        '#default_value' => $webform->getThirdPartySetting('webform_jira_service_desk', 'status'),
      ];

      $form['jira_configuration']['save_issue_key'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Save issue key to Webform Submission'),
        '#default_value' => $webform->getThirdPartySetting('webform_jira_service_desk', 'save_issue_key'),
      ];

      $form['jira_configuration']['issue_key_webform_element'] = [
        '#type' => 'select',
        '#title' => $this->t('Webform element'),
        '#options' => $this->getWebformElementOptions(),
        '#default_value' => $webform->getThirdPartySetting('webform_jira_service_desk', 'issue_key_webform_element'),
        '#empty_option' => $this->t('- None -'),
        '#description' => $this->t('Select the Webform element for saving the JIRA Issue key.'),
        '#states' => [
          'visible' => [
            ':input[name="save_issue_key"]' => ['checked' => TRUE],
          ],
          'required' => [
            ':input[name="save_issue_key"]' => ['checked' => TRUE],
          ],
        ],
      ];

      $form['jira_configuration']['servicedesk_id'] = [
        '#type' => 'number',
        '#title' => $this->t('Service Desk'),
        '#default_value' => $webform->getThirdPartySetting('webform_jira_service_desk', 'servicedesk_id'),
        '#description' => $this->t('Select the Service Desk to send the submission to.'),
        '#required' => TRUE,
      ];

      $selectedServiceDesk = $form_state->getValue('servicedesk_id') ?: $webform->getThirdPartySetting('webform_jira_service_desk', 'servicedesk_id');

      $form['jira_configuration']['request_type_id'] = [
        '#type' => 'number',
        '#title' => $this->t('Request Type'),
        '#default_value' => $webform->getThirdPartySetting('webform_jira_service_desk', 'request_type_id'),
        '#description' => $this->t('Select the Request Type to submit.'),
        '#required' => TRUE,
        '#states' => [
          '!visible' => [
            ':input[name="servicedesk_id"]' => ['value' => ''],
          ],
        ],
        '#prefix' => '<div id="field-request-type">',
        '#suffix' => '</div>',
      ];

      $form['jira_configuration']['check_for_duplicates'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Check for duplicate tickets'),
        '#default_value' => $webform->getThirdPartySetting('webform_jira_service_desk', 'check_for_duplicates'),
        '#description' => $this->t('Verify if ticket exists before sending request. This is useful if your jira instance is slow to respond. Only string field types are usable for queries at this point.'),
      ];

      $selectedRequestType = $form_state->getValue('request_type_id') ?: $webform->getThirdPartySetting('webform_jira_service_desk', 'request_type_id');

      $options_duplicate_fields = $this->getDuplicateFields($selectedServiceDesk, $selectedRequestType);

      $form['jira_configuration']['duplicate_fields'] = [
        '#type' => 'select',
        '#title' => $this->t('Duplicate field check'),
        '#description' => $this->t('Field to check to see if form already exists. The webform value will be taken from mapping below'),
        '#options' => $options_duplicate_fields,
        '#default_value' => $webform->getThirdPartySetting('webform_jira_service_desk', 'duplicate_fields'),
        '#multiple' => TRUE,
        '#states' => [
          'visible' => [
            ':input[name="check_for_duplicates"]' => ['checked' => TRUE],
          ],
          'required' => [
            ':input[name="check_for_duplicates"]' => ['checked' => TRUE],
          ],
        ]
      ];

      $form['webform_jira_service_desk_fields'] = [
        '#type' => 'table',
        '#header' => $this->getTableHeader(),
        '#empty' => $this->t('Please add JIRA Service Desk mapping fields.'),
        '#attributes' => [
          'class' => ['webform-jira-table'],
        ],
      ] + $this->getTableRows();

      WebformDialogHelper::attachLibraries($form);

      $form['#attached']['library'][] = 'webform_ui/webform_ui';
      $form = parent::buildForm($form, $form_state);
    }
    else {
      $this->messenger()
        ->addError($this->t('Error connecting to JIRA.'));
    }

    return $this->buildAjaxForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if (
      $form_state->getValue('save_issue_key') &&
      empty($form_state->getValue('issue_key_webform_element'))
    ) {
      $form_state->setErrorByName(
        'issue_key_webform_element',
        $this->t('Field Webform element can not be empty if Save issue key to Webform submission field is selected.')
      );
    }

    parent::validateForm($form, $form_state);
  }

  /**
   * Re-render the request type select options.
   *
   * When the service desk option is changed.
   *
   * @return array
   *   Request Type Webform Element.
   */
  public function serviceDeskCallback(array $form, FormStateInterface $form_state) {
    return $form['jira_configuration']['request_type_id'];
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\webform\WebformInterface $webform */
    $webform = $this->getEntity();

    $webform->setThirdPartySetting(
      'webform_jira_service_desk',
      'status',
      (bool) $form_state->getValue('jira_status')
    );

    $webform->setThirdPartySetting(
      'webform_jira_service_desk',
      'save_issue_key',
      (bool) $form_state->getValue('save_issue_key')
    );

    $webform->setThirdPartySetting(
      'webform_jira_service_desk',
      'issue_key_webform_element',
      $form_state->getValue('issue_key_webform_element')
    );

    $webform->setThirdPartySetting(
      'webform_jira_service_desk',
      'servicedesk_id',
      $form_state->getValue('servicedesk_id')
    );

    $webform->setThirdPartySetting(
      'webform_jira_service_desk',
      'request_type_id',
      $form_state->getValue('request_type_id')
    );

    $webform->setThirdPartySetting(
      'webform_jira_service_desk',
      'check_for_duplicates',
      (bool) $form_state->getValue('check_for_duplicates')
    );

    $webform->setThirdPartySetting(
      'webform_jira_service_desk',
      'duplicate_fields',
      $form_state->getValue('duplicate_fields')
    );

    $this->setEntity($webform);

    $this->messenger()
      ->addStatus($this->t('Jira fields for Webform @label saved.', [
        '@label' => $webform->label(),
      ]));

    return parent::save($form, $form_state);
  }

  /**
   * Gets the Jira table header.
   *
   * @return array
   *   The header elements.
   */
  protected function getTableHeader() {
    $header = [];
    $header['key'] = $this->t('Key');
    $header['label'] = $this->t('Label');
    $header['jira_field_id'] = $this->t('Jira Field id');
    $header['jira_field_type'] = $this->t('Jira Field Type');
    $header['value_type'] = $this->t('Value type');
    $header['Value'] = $this->t('Value');
    $header['operations'] = [
      'data' => $this->t('Operations'),
      'class' => ['webform-jira-operations'],
    ];
    return $header;
  }

  /**
   * Gets the elements table rows for each field.
   *
   * @return array
   *   The header elements.
   */
  protected function getTableRows() {
    /** @var \Drupal\webform\WebformInterface $webform */
    $webform = $this->getEntity();
    $fields = (array) WebformYaml::decode($webform->getThirdPartySetting(
      'webform_jira_service_desk',
      'fields'
    ));

    $rows = [];
    foreach ($fields as $key => $field) {
      $rows[$key] = [
        'key' => ['#markup' => $key],
        'label' => ['#markup' => $field['label']],
        'field_id' => ['#markup' => $field['field_id']],
        'field_type' => ['#markup' => $field['field_type']],
        'value_type' => ['#markup' => $field['value_type']],
        'value' => ['#markup' => $field['value']],
        'operations' => [
          '#type' => 'operations',
          '#prefix' => '<div class="webform-dropbutton">',
          '#suffix' => '</div>',
          '#links' => [
            'edit' => [
              'title' => $this->t('Edit'),
              'url' => new Url('webform_jira_service_desk.edit_field', [
                'webform' => $webform->id(),
                'key' => $key,
              ]),
              'attributes' => WebformDialogHelper::getModalDialogAttributes(),
            ],
          ],
        ],
      ];
    }
    return $rows;
  }

  /**
   * Get element options.
   *
   * @return array
   *   Element options.
   */
  protected function getWebformElementOptions() {
    $element_options = [];
    /** @var \Drupal\webform\WebformInterface $webform */
    $webform = $this->getEntity();
    $decoded_elements = $webform->getElementsDecodedAndFlattened();
    if ($decoded_elements) {
      foreach ($decoded_elements as $key => $element) {
        $element_options[$key] = isset($element['#title']) ? Unicode::truncate($element['#title'], 30, TRUE, TRUE) . ' (key: ' . $key . ')' : $element['#type'];
      }
    }
    return $element_options;
  }

  /**
   * Load the Service Desk options.
   *
   * @return array
   *   Return.
   */
  protected function getServiceDeskOptions() {
    /** @var \Drupal\webform_jira_service_desk\JiraServiceDeskService */
    $jira = \Drupal::service('webform_jira_service_desk.jira_service');
    return $jira->getAvailableServiceDesks();
  }

  /**
   * Load the Request Type options.
   *
   * @param int|string $id
   *   Id.
   *
   * @return array
   *   Return.
   */
  protected function getRequestTypeOptions($id) {
    /** @var \Drupal\webform_jira_service_desk\JiraServiceDeskService */
    $jira = \Drupal::service('webform_jira_service_desk.jira_service');
    return $jira->getAvailableRequestTypes($id);
  }

  /**
   * {@inheritdoc}
   */
  protected function actionsElement(array $form, FormStateInterface $form_state) {
    $form = parent::actionsElement($form, $form_state);
    $form['submit']['#value'] = $this->t('Save Jira settings');
    unset($form['delete']);
    unset($form['reset']);
    return $form;
  }

  /**
   * Returns an list of fields that can be used to verify duplicates.
   *
   * @param int|string $serviceDeskId
   *  Service desk ID
   * @param int|string $requestTypeId
   *  Request Type ID
   *
   * @return array
   */
  protected function getDuplicateFields($serviceDeskId, $requestTypeId) {
    $jira = \Drupal::service('webform_jira_service_desk.jira_service');
    $jiraFields = $jira->getRequestTypeFields($serviceDeskId, $requestTypeId);
    $options = [];
    foreach ($jiraFields as $f) {
      // Only string field types are usable for queries at this point.
      if ($f['type'] === 'string') {
        $options[$f['id']] = $f['display'];
      }
    }
    return $options;
  }

}
