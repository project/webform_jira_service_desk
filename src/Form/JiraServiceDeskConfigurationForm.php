<?php

namespace Drupal\webform_jira_service_desk\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\Messenger;
use Drupal\key\KeyRepositoryInterface;
use Drupal\key\Plugin\KeyType\UserPasswordKeyType;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Exception\RequestException;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class JiraServiceConfiguration.
 *
 * @package Drupal\webform_jira_service_desk_service_desk\Form
 */
class JiraServiceDeskConfigurationForm extends ConfigFormBase {

  /**
   * The key repository.
   *
   * @var \Drupal\key\KeyRepositoryInterface
   */
  protected $keyRepository;

  /**
   * The messenger.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The HTTP client.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * JiraServiceConfiguration constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Config factory.
   * @param \Drupal\key\KeyRepositoryInterface $key_repository
   *   Key repository.
   * @param \Drupal\Core\Messenger\Messenger $messenger
   *   Messenger.
   * @param \GuzzleHttp\Client $http_client
   *   Guzzle client.
   */
  public function __construct(ConfigFactoryInterface $config_factory, KeyRepositoryInterface $key_repository, Messenger $messenger, Client $http_client) {
    $this->keyRepository = $key_repository;
    $this->messenger = $messenger;
    $this->httpClient = $http_client;
    parent::__construct($config_factory);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('key.repository'),
      $container->get('messenger'),
      $container->get('http_client')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'webform_jira_service_desk_service_configuration_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'webform_jira_service_desk.service_configuration',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('webform_jira_service_desk.service_configuration');

    $form['host'] = [
      '#title' => $this->t('Jira host'),
      '#description' => $this->t('Url of Jira instance'),
      '#type' => 'textfield',
      '#required' => TRUE,
      '#default_value' => $config->get('host'),
    ];

    $form['user_password_key'] = [
      '#type' => 'key_select',
      '#title' => $this->t('User/Password key'),
      '#description' => $this->t('<br>Select the key with username/password combination in json format. For example {"username":"JIRA_USERNAME", "password":"JIRA_API_TOKEN"}'),
      '#required' => TRUE,
      '#default_value' => $config->get('user_password_key'),
    ];

    $form['retry_codes'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Retry codes'),
      '#description' => $this->t("A list of HTTP error codes for which the request should be re-queued automatically. Each code should be on a new line."),
      '#required' => TRUE,
      '#default_value' => $config->get('retry_codes'),
    ];

    $form['debug_requests'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Debug requests'),
      '#description' => $this->t("Log full request payload. Please ensure that requests do not contain any personal information that is being logged to an insecure location."),
      '#default_value' => $config->get('debug_requests'),
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $user_password_key = $form_state->getValue('user_password_key');
    /** @var \Drupal\key\KeyInterface $key_entity */
    $key_entity = $this->keyRepository->getKey($user_password_key);
    if (!$key_entity->getKeyType() instanceof UserPasswordKeyType) {
      $form_state->setErrorByName('user_password_key', $this->t('The key should be a Username/password type.'));
      return;
    }

    $key_values = $key_entity->getKeyValues();
    try {
      $this->httpClient->get($form_state->getValue('host') . 'rest/api/2/myself', [
        'auth' => [$key_values['username'], $key_values['password']],
      ]);
    }
    catch (ConnectException $e) {
      $this->messenger->addError($e->getMessage());
    }
    catch (RequestException $e) {
      $this->messenger->addError($e->getMessage());
    }

    $this->messenger->addStatus($this->t('Successfully retrieved API information.'));

    $retry_codes = explode("\r\n", $form_state->getValue('retry_codes'));
    foreach ($retry_codes as $retry_code) {
      if (!preg_match('/^\d{3}$/', $retry_code)) {
        $form_state->setErrorByName('retry_codes', $this->t('Each code should be on a new line and should be a 3 digit HTTP response code'));
      }
    }

    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $this->config('webform_jira_service_desk.service_configuration')
      ->set('host', $values['host'])
      ->set('user_password_key', $values['user_password_key'])
      ->set('retry_codes', $values['retry_codes'])
      ->set('debug_requests', $values['debug_requests'])
      ->save();

    parent::submitForm($form, $form_state);
  }

}
