<?php

namespace Drupal\webform_jira_service_desk;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Http\ClientFactory;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\webform\Plugin\WebformElementManagerInterface;
use Drupal\webform\Utility\WebformYaml;
use Drupal\webform\WebformSubmissionInterface;
use Drupal\webform\WebformTokenManagerInterface;
use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\Exception\RequestException;
use Drupal\key\KeyRepositoryInterface;

/**
 * Integration with JIRA Service Desk REST API.
 *
 * Allows the creation of requests in JIRA Service Desk.
 *
 * Some utility functions to provide readable information on Service Desks,
 * Request Types, field names etc
 *
 * @package Drupal\webform_jira
 */
class JiraServiceDeskService {
  use StringTranslationTrait;

  /**
   * Logger channel.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * Webform token manager.
   *
   * @var \Drupal\webform\WebformTokenManagerInterface
   */
  protected $webformTokenManager;

  /**
   * Webform element manager.
   *
   * @var \Drupal\webform\Plugin\WebformElementManagerInterface
   */
  protected $webformElementManager;

  /**
   * Messenger Interface.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * HTTP client.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * Config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $config;

  /**
   * Repository for Key configuration entities.
   *
   * @var \Drupal\key\KeyRepositoryInterface
   */
  protected $keyRepository;

  /**
   * Client Factory for HTTP Service
   *
   * @var \Drupal\Core\Http\ClientFactory
   */
  protected $httpClientFactory;

  /**
   * JiraServiceDeskService constructor.
   *
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   Logger factory.
   * @param \Drupal\webform\WebformTokenManagerInterface $webform_token_manager
   *   Webform token manager.
   * @param \Drupal\webform\Plugin\WebformElementManagerInterface $webform_element_manager
   *   Webform element manager.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   Messenger interface.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config
   *   Config factory.
   * @param \Drupal\key\KeyRepositoryInterface $keyRepository
   *   Repository for Key configuration entities.
   * @param \Drupal\Core\Http\ClientFactory $httpClientFactory
   *   HTTP Client Factory.
   */
  public function __construct(
    LoggerChannelFactoryInterface $logger_factory,
    WebformTokenManagerInterface $webform_token_manager,
    WebformElementManagerInterface $webform_element_manager,
    MessengerInterface $messenger,
    ConfigFactoryInterface $config,
    KeyRepositoryInterface $keyRepository,
    ClientFactory $httpClientFactory
  ) {
    $this->logger = $logger_factory->get('webform_jira_service_desk');
    $this->webformTokenManager = $webform_token_manager;
    $this->webformElementManager = $webform_element_manager;
    $this->messenger = $messenger;
    $this->config = $config;
    $this->keyRepository = $keyRepository;
    $this->httpClientFactory = $httpClientFactory;
    $this->configureClient();
  }

  /**
   * Get list of Service Desks exposed in JIRA.
   *
   * @return array
   *   of id => name pairs.
   */
  public function getAvailableServiceDesks() {
    try {
      $response = $this->httpClient->request('GET', 'rest/servicedeskapi/servicedesk');
      $response_data = Json::decode($response->getBody());

      $desks = [];
      foreach ($response_data['values'] as $value) {
        $desks[$value['id']] = $value['projectName'];
      }
      return $desks;
    }
    catch (BadResponseException $e) {
      $this->logger->error($e->getMessage());
      return [];
    }
    catch (RequestException $e) {
      $this->logger->error($e->getMessage());
      $this->messenger->addError("Something went wrong and your request has not been submitted. Please try again later.");
    }
  }

  /**
   * Get Request Types from JIRA.
   *
   * @param int|string $serviceDeskId
   *   Service desk id.
   *
   * @return array
   *   of id => name pairs.
   */
  public function getAvailableRequestTypes($serviceDeskId) {
    try {
      $response = $this->httpClient->request('GET', 'rest/servicedeskapi/servicedesk/' . $serviceDeskId . '/requesttype');
      $response_data = Json::decode($response->getBody());

      $types = [];
      foreach ($response_data['values'] as $value) {
        $types[$value['id']] = $value['name'];
      }
      return $types;
    }
    catch (BadResponseException $e) {
      // Assuming that Guzzle has logged the errors here.
      return [];
    }
    catch (RequestException $e) {
      $this->logger->error($e->getMessage());
      $this->messenger->addError("Something went wrong and your request has not been submitted. Please try again later.");
    }

  }

  /**
   * Get field information for the request type.
   *
   * @param int|string $serviceDeskId
   *   Service Desk ID.
   * @param int|string $requestTypeId
   *   Request type ID.
   *
   * @return array
   *   With id, display, type keys.
   */
  public function getRequestTypeFields($serviceDeskId, $requestTypeId) {
    try {
      $response = $this->httpClient->request('GET', 'rest/servicedeskapi/servicedesk/' . $serviceDeskId . '/requesttype/' . $requestTypeId . '/field');
      $response_data = Json::decode($response->getBody());

      $fields = [];
      foreach ($response_data['requestTypeFields'] as $value) {
        $fields[] = [
          'id' => $value['fieldId'],
          'display' => $value['name'],
          'type' => $value['jiraSchema']['type'],
        ];
      }
      return $fields;
    }
    catch (BadResponseException $e) {
      // Assuming that Guzzle has logged the errors here.
      return [];
    }

  }

  /**
   * Formats and submits the webform submission to JIRA Service Desk.
   *
   * @param \Drupal\webform\WebformSubmissionInterface $webform_submission
   *   Webform submission.
   *
   * @return \Psr\Http\Message\ResponseInterface|RequestException|null
   *   Request exception.
   */
  public function createRequestFromSubmission(WebformSubmissionInterface $webform_submission) {
    $webform = $webform_submission->getWebform();
    $config = $this->config->get('webform_jira_service_desk.service_configuration');

    $fields = (array) WebformYaml::decode($webform->getThirdPartySetting(
      'webform_jira_service_desk',
      'fields'
    ));

    if ($fields) {
      $post_data = [
        "serviceDeskId" => $webform->getThirdPartySetting('webform_jira_service_desk', 'servicedesk_id'),
        "requestTypeId" => $webform->getThirdPartySetting('webform_jira_service_desk', 'request_type_id'),
        "requestFieldValues" => $this->mapFormFieldsToPostData($webform_submission, $fields),
      ];

      try {
        if ($config->get('debug_requests')) {

          $this->logger->debug('Sending request for submission @submission_id to Jira with data:<br/> @postdata',
            [
              '@postdata' => Json::encode($post_data),
              '@submission_id' => $webform_submission->id(),
            ]);
        }
        $response = $this->httpClient->request('POST',
          'rest/servicedeskapi/request',
          ['json' => $post_data]
        );
        return $response;
      }
      catch (RequestException $e) {
        $message = 'Webform submission with ID: ' . $webform_submission->id() . ' failed </br>Error message: ' . $e->getMessage();
        $this->logger->error($message);
        $this->messenger->addWarning("Something went wrong and your request has not been submitted. Please try again later.");
        return $e;
      }
    }
    else {
      $this->logger->warning("Webform @title has JIRA Service Desk enabled with no fields set", ['@title' => $webform->label()]);
    }

    return NULL;
  }

  /**
   * Maps Submission data to the format and names that JIRA is expecting.
   *
   * @param \Drupal\webform\WebformSubmissionInterface $submission
   *   Webform submission.
   * @param array $fields
   *   Fields.
   *
   * @return array
   *   Array formatter fields to send to jira.
   */
  protected function mapFormFieldsToPostData(WebformSubmissionInterface $submission, array $fields) {
    $formatted = [];
    foreach ($fields as $field) {
      if ($value = $this->processFieldAndSubmissionDataToValue($field, $submission)) {
        $formatted[$field['field_id']] = $value;
      }
    }
    return $formatted;
  }

  /**
   * Converts an individual field to correct data type and field name for JIRA.
   *
   * @param array $field
   *   Field.
   * @param \Drupal\webform\WebformSubmissionInterface $webform_submission
   *   Webform submission.
   *
   * @return array|null|string
   *   Correct data type for jira.
   *
   * @throws \Exception
   */
  protected function processFieldAndSubmissionDataToValue(array $field, WebformSubmissionInterface $webform_submission) {
    if (empty($field['field_type'])) {
      throw new \Exception('field_type not specified in field');
    }

    if ($value = $this->getValueByValueType($field, $webform_submission)) {
      if ($field['field_type'] === 'string') {
        return $value;
      }
      if ($field['field_type'] === 'option') {
        return ['value' => $value];
      }
      if ($field['field_type'] === 'option_multiple') {
        $list = [];
        foreach ($value as $option) {
          $list[] = ['value' => $option];
        }
        return $list;
      }
      if ($field['field_type'] === 'array') {
        $list = [];
        if (is_array($value)) {
          foreach ($value as $option) {
            $list[] = ['value' => $option];
          }
          return $list;
        }
        else {
          $list[] = ['value' => $value];
          return $list;
        }
      }
      if ($field['field_type'] === 'number') {
        return floatval(
          preg_replace("/[^-0-9\.]/", "", $value)
        );
      }
      if ($field['field_type'] === 'any') {
        if (is_array($value)) {
          return implode(',', $value);
        }
        else {
          return $value;
        }
      }
      if ($field['field_type'] === 'datetime') {
        // Assumes datetime is coming from the webform in a usable format.
        // Format should be "2022-10-26T13:49:51.00+1100",
        // (note the miliseconds).
        return $value;
      }
      if ($field['field_type'] === 'date') {
        // Using a webform date field should be sufficient.
        return $value;
      }

      throw new \Exception('field_type not found ' . $field['field_type']);
    }
  }

  /**
   * Gets the value of the field as the correct type.
   *
   * Will process tokens as required.
   *
   * @param array $field
   *   Field.
   * @param \Drupal\webform\WebformSubmissionInterface $webform_submission
   *   Webform submission.
   *
   * @return array|null|string
   *   Correct data type for jira.
   */
  protected function getValueByValueType(array $field, WebformSubmissionInterface $webform_submission) {
    $value = NULL;
    $data = $webform_submission->getData();
    if (!empty($field['value_type'])) {
      $value_type = $field['value_type'];
      if ($value_type == 'webform_element' && !empty($data[$field['value']])) {
        $value = $data[$field['value']];
      }
      if ($value_type == 'text' && !empty($field['value'])) {
        $value = $this->webformTokenManager->replace(
          $field['value'], $webform_submission, [], ['clear' => TRUE]
        );
      }
    }

    return $value;
  }

  /**
   * Configure httpClient,  settings from JiraServiceDeskConfigurationForm.
   */
  protected function configureClient() {
    $config = $this->config->get('webform_jira_service_desk.service_configuration');

    $base_url = $config->get('host');
    $key_name = $config->get('user_password_key');
    $auth = $this->keyRepository
      ->getKey($key_name)
      ->getKeyValues();

    $this->httpClient = $this->httpClientFactory
      ->fromOptions([
        'base_uri' => $base_url,
        'auth' => [$auth['username'], $auth['password']],
      ]);
  }

  /**
   *
   * Checks if ticket already exists on your Jira instance.
   *
   * @param WebformSubmissionInterface $webform_submission
   *  Webform Submission
   *
   * @return array|null
   *  Returns array of issue information from jira or null if no issue exists.
   */
  public function checkForDuplicates(WebformSubmissionInterface $webform_submission) {
    $webform = $webform_submission->getWebform();

    $fields = (array) WebformYaml::decode($webform->getThirdPartySetting(
      'webform_jira_service_desk',
      'fields'
    ));
    $field_configs = array_combine(array_column($fields, 'field_id'), $fields);

    $duplicate_fields = $webform->getThirdPartySetting('webform_jira_service_desk', 'duplicate_fields');

    $index = 0;
    $post_data = [];
    foreach ($duplicate_fields as $duplicate_field_name) {
      $duplicate_field = $field_configs[$duplicate_field_name];
      // Assumes custom field name will be of form 'customfield_1234'.
      $custom_field_number = substr($duplicate_field_name, strlen('customfield_'));
      if ($duplicate_field['value_type'] === 'webform_element') {
        $custom_field_value = '"' . $webform_submission->getElementData($duplicate_field['value']) . '"';
      }
      else {
        $custom_field_value = $this->webformTokenManager->replace(
          $duplicate_field['value'], $webform_submission, [], ['clear' => TRUE]
        );
      }

      if ($index == 0) {
        $post_data = [
          'jql' => 'cf[' . $custom_field_number . '] ~ ' . $custom_field_value,
        ];
      }
      else {
        $post_data['jql'] .= ' AND cf[' . $custom_field_number . '] ~ ' . $custom_field_value;
      }
      $index++;
    }

    try {
      $response = $this->httpClient->request('POST',
        'rest/api/2/search',
        ['json' => $post_data]
      );
      $response_data = Json::decode($response->getBody());
      if(!empty($response_data['issues'])) {
        // Use first response, there should 'never' be more than one.
        return $response_data['issues'][0];
      }
    } catch (RequestException $e) {
      $this->logger->error('There was an error checking for duplicate Jira tickets. Webform submission @submission_id @error_message', [
        '@submission_id' => $webform_submission->id(),
        '@error_message' => $e->getMessage(),
      ]);
    }
  }

}
