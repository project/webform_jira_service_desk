<?php

namespace Drupal\webform_jira_service_desk\Plugin\QueueWorker;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueFactory;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\webform_jira_service_desk\JiraServiceDeskQueueHealthService;
use GuzzleHttp\Exception\RequestException;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Queue\RequeueException;
use GuzzleHttp\Psr7\Response;

/**
 * Send Jira Request.
 *
 * @QueueWorker(
 *   id = "cron_jira_request_queue",
 *   title = @Translation("Jira Request Queue Worker."),
 *   cron = {"time" = 60}
 * )
 */
class SendJiraRequest extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * Logger channel.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * The queue object.
   *
   * @var \Drupal\Core\Queue\QueueInterface
   */
  protected $queue;

  /**
   * Config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Queue Health Service.
   *
   * @var \Drupal\webform_jira_service_desk\JiraServiceDeskQueueHealthService
   */
  protected $queueHealthService;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, QueueFactory $queue_factory, LoggerChannelFactoryInterface $logger_factory, ConfigFactoryInterface $config_factory, JiraServiceDeskQueueHealthService $queueHealthService) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->queue = $queue_factory->get('cron_jira_request_queue');
    $this->logger = $logger_factory->get('webform_jira_service_desk');
    $this->configFactory = $config_factory;
    $this->queueHealthService = $queueHealthService;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    /** @var \Drupal\Core\Queue\QueueFactory $queueFactory */
    $queue_factory = $container->get('queue');
    $logger_factory = $container->get('logger.factory');
    $config_factory = $container->get('config.factory');
    $queueHealthService = $container->get('webform_jira_service_desk.queue_health_service');
    return new static($configuration, $plugin_id, $plugin_definition, $queue_factory, $logger_factory, $config_factory, $queueHealthService);
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($webform_submission) {
    // Prevent errors if for some reason $webform_submission is missing.
    if (!$webform_submission) {
      return;
    }
    /** @var \Drupal\webform\WebformInterface $webform */
    $webform = $webform_submission->getWebform();

    // Only send information if webform_jira_service_desk is enabled.
    // This is a secondary check, request should never be queued.
    if ($webform->getThirdPartySetting('webform_jira_service_desk', 'status')) {
      /** @var \Drupal\webform_jira_service_desk\JiraServiceDeskService $jira_service_desk_service */
      $jira_service_desk_service = \Drupal::service('webform_jira_service_desk.jira_service');

      if($webform->getThirdPartySetting('webform_jira_service_desk', 'check_for_duplicates')) {
        $issue = $jira_service_desk_service->checkForDuplicates($webform_submission);
      }

      if (isset($issue)) {
        $this->logger->notice('Submission already exists on Jira. Submission ID: @submission_id, JIRA ID: @jira_id', [
          '@submission_id' => $webform_submission->id(),
          '@jira_id' => $issue['key'],
        ]);

        // Save Jira ticket ID to webform.
        if ($webform->getThirdPartySetting('webform_jira_service_desk', 'save_issue_key')) {
          $webform_submission->setElementData(
            $webform->getThirdPartySetting('webform_jira_service_desk', 'issue_key_webform_element'),
            $issue['key']
          );
          $webform_submission->save();
        }
        return;
      }

      $response = $jira_service_desk_service->createRequestFromSubmission($webform_submission);

      // Successful Request.
      if ($response instanceof Response) {
        if ($response->getStatusCode() == 201) {
          $response_data = Json::decode($response->getBody());
          $this->logger->notice('Webform Submission with ID: @submission_id submitted to JIRA as @jira_id', [
            '@submission_id' => $webform_submission->id(),
            '@jira_id' => $response_data['issueKey'],
          ]);

          // Save Jira ticket ID to webform.
          if ($response_data && $webform->getThirdPartySetting('webform_jira_service_desk', 'save_issue_key')) {
            $webform_submission->setElementData(
              $webform->getThirdPartySetting('webform_jira_service_desk', 'issue_key_webform_element'),
              $response_data['issueKey']
            );
            $webform_submission->save();
          }
        }
      }

      // Bad response.
      if ($response instanceof RequestException) {
        $retry_codes = preg_split('/(\r\n?|\n)/', $this->configFactory->get('webform_jira_service_desk.service_configuration')->get('retry_codes'));
        // Requeue if this is a response type that can be saved.
        if (in_array($response->getCode(), $retry_codes)) {
          throw new RequeueException($response->getCode() . ' Error from Jira, requeue request.');
        }
      }

      // Log response.
      $this->queueHealthService->writeSendToJiraResult($webform_submission, $response);
    }
    else {
      // We should do something if webform_jira_service_desk is disabled.
      // For the moment log it.
      $this->queueHealthService->writeSendToJiraResult($webform_submission);
    }
  }

}
