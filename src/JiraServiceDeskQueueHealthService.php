<?php

namespace Drupal\webform_jira_service_desk;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Database\Connection;
use Drupal\Core\Queue\QueueFactory;
use Drupal\webform\WebformSubmissionInterface;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7\Response;

/**
 * Service for jira queue health.
 */
class JiraServiceDeskQueueHealthService {

  /**
   * Queue Factory.
   *
   * @var \Drupal\Core\Queue\QueueFactory
   */
  protected $queueFactory;

  /**
   * Database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * Construct service.
   *
   * @param \Drupal\Core\Database\Connection $connection
   *   Database connection.
   * @param \Drupal\Core\Queue\QueueFactory $queueFactory
   *   Queue Factory.
   */
  public function __construct(Connection $connection, QueueFactory $queueFactory) {
    $this->connection = $connection;
    $this->queueFactory = $queueFactory;
  }

  /**
   * Get last success.
   */
  public function getLastSuccess() {
    $select = $this->connection
      ->select('webform_jira_service_history')
      ->fields('webform_jira_service_history', ['timestamp'])
      ->condition('response_code', 201)
      ->orderBy('timestamp', 'DESC')
      ->range(0, 1);

    $result = $select->execute()->fetchField();
    return $result;
  }

  /**
   * Get send failures.
   */
  public function getResendableFailures() {
    $success = $this->connection
      ->select('webform_jira_service_history', 'sh')
      ->fields('sh', ['webform_sid'])
      ->condition('response_code', 201);

    $select = $this->connection
      ->select('webform_jira_service_history', 'sh');
    $select->fields('sh', [
      'timestamp',
      'webform_form',
      'webform_sid',
      'response_code',
      'response_message',
    ]);
    if (!empty($success->execute()->fetchCol())) {
      $select->where('webform_sid NOT IN (:successes[])', [
        ':successes[]' => $success->execute()
          ->fetchCol(),
      ]);

    }

    $group = $select->orConditionGroup()
      ->isNull('response_code')
      ->condition('response_code', 201, '!=');

    $select->condition($group)
      ->orderBy('timestamp', 'ASC');

    $result = $select->execute()->fetchAll();
    return $result;
  }

  /**
   * Get send failures.
   */
  public function getAllSendFailures() {
    $select = $this->connection
      ->select('webform_jira_service_history')
      ->fields('webform_jira_service_history', [
        'timestamp',
        'webform_form',
        'webform_sid',
        'response_code',
        'response_message',
      ]);

    $group = $select->orConditionGroup()
      ->isNull('response_code')
      ->condition('response_code', 201, '!=');

    $select->condition($group)
      ->orderBy('timestamp', 'ASC');

    $result = $select->execute()->fetchAll();
    return $result;
  }

  /**
   * Log result of send to jira.
   *
   * @param \Drupal\webform\WebformSubmissionInterface $webform_submission
   *   Webform submission.
   * @param \GuzzleHttp\Psr7\Response|\GuzzleHttp\Exception\RequestException $response
   *   Response of request.
   */
  public function writeSendToJiraResult(WebformSubmissionInterface $webform_submission, $response = NULL) {
    $fields = [
      'uid' => $webform_submission->getOwnerId(),
      'webform_form' => $webform_submission->getWebform()->id(),
      'webform_sid' => $webform_submission->id(),
      'timestamp' => time(),
    ];

    if ($response instanceof Response) {
      $fields['response_code'] = $response->getStatusCode();
      if ($response->getStatusCode() == 201) {
        $response_data = Json::decode($response->getBody());
        $fields['jira_ticket'] = $response_data['issueKey'];
      }
    }

    if ($response instanceof RequestException) {
      $fields['response_code'] = $response->getCode();
      $fields['response_message'] = $response->getMessage();
    }
    $this->connection->insert('webform_jira_service_history')
      ->fields($fields)
      ->execute();
  }

  /**
   * Returns the number of Items remaining in the queue.
   */
  public function getRemainingCount() {
    $queue = $this->queueFactory->get('cron_jira_request_queue');
    return $queue->numberOfItems();
  }

  /**
   * Returns the created timestamp of the oldest item in the queue.
   */
  public function getOldestItemCreated() {
    $queue = $this->queueFactory->get('cron_jira_request_queue');
    $next_item = $queue->claimItem();
    if ($next_item) {
      $queue->releaseItem($next_item);
      return $next_item->created;
    }
  }

}
